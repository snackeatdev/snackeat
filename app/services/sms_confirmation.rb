class SmsConfirmation
  attr_reader :message, :to

  def initialize(sms_code, to)
    @sms_code = sms_code
    @to = "+91" + to
  end

  def call
    client = Twilio::REST::Client.new APP_CONFIG.account_sid, APP_CONFIG.auth_token
    client.api.account.messages.create(
      :from => "+44 7400 299883",
      :to => @to,
      :body => "Thank you for joining SnackEat. Your mobile verification code is #{@sms_code}"
    )
  end
end
