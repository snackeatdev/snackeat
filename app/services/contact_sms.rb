class ContactSms

  def initialize(current_user, recipient, url)
    @recipient = recipient
    @url = url
    @current_user = current_user.display_name
    @to = "+91" + @recipient.phone_number
    @body = conversion_body
  end

  def call
    client = Twilio::REST::Client.new APP_CONFIG.account_sid, APP_CONFIG.auth_token
    if @recipient.phone_number.present?
      client.api.account.messages.create(
        :from => "+44 7400 299883",
        :to => @to,
        :body => @body
      )
    end
  end

  def conversion_body
    "Hello #{@recipient.given_name}, #{@current_user} has sent you a message in SnackEat. Inbox link - #{@url}"
  end
end
