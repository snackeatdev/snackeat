# == Schema Information
#
# Table name: listing_likes
#
#  id           :integer          not null, primary key
#  author_id    :string(255)
#  listing_id   :integer
#  community_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class ListingLike < ActiveRecord::Base
	belongs_to :listing
end
