# == Schema Information
#
# Table name: profile_likes
#
#  id           :integer          not null, primary key
#  author_id    :string(255)
#  profile_id   :string(255)
#  community_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class ProfileLike < ActiveRecord::Base
	belongs_to :author, :class_name => "Person", :foreign_key => "author_id"
end
