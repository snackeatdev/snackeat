class CreateListingLikes < ActiveRecord::Migration[5.1]
  def change
    create_table :listing_likes do |t|
      t.string :author_id
      t.integer :listing_id
      t.integer :community_id
      t.timestamps
    end
  end
end
