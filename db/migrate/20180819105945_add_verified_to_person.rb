class AddVerifiedToPerson < ActiveRecord::Migration[5.1]
  def change
    add_column :people, :pin, :integer
    add_column :people, :pin_sent_at, :datetime
    add_column :people, :verified, :boolean, default: false
  end
end
