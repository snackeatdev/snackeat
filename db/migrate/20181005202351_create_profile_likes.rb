class CreateProfileLikes < ActiveRecord::Migration[5.1]
  def change
    create_table :profile_likes do |t|
      t.string :author_id
      t.string :profile_id
      t.integer :community_id
      t.timestamps
    end
  end
end
